import React from 'react';

const AnalTestComponent = () => {
    const questions = [
        {   // Q1 변의 모양
            questions : "최근 3일 간 변의 모양을 선택해 주세요.",
            answer : [
                { imgUrl: "", text: "구슬 변", score: 4 },
                { imgUrl: "", text: "꽈배기 변", score: 3 },
                { imgUrl: "", text: "솔방울 표면 변", score: 2 },
                { imgUrl: "", text: "매끄러운 소세지 변", score: 1 },
                { imgUrl: "", text: "조각 조각 묽은 변", score: 2 },
                { imgUrl: "", text: "묽은 변", score: 3 },
                { imgUrl: "", text: "액체 변", score: 4 },
                { imgUrl: "", text: "변을 보지 않음", score: 5 }
            ],
            pageNum : 1
        },
        {   // Q2 변의 색깔
            questions : "최근 3일 간 변의 색깔에 대해서 선택해 주세요.",
            answer : [
                { imgUrl: "", text: "옅은 갈색", score: 1 },
                { imgUrl: "", text: "갈색", score: 1 },
                { imgUrl: "", text: "녹색", score: 2 },
                { imgUrl: "", text: "노란색", score: 3 },
                { imgUrl: "", text: "빨간색", score: 5 },
                { imgUrl: "", text: "검은색", score: 5 },
                { imgUrl: "", text: "피섞인 갈색", score: 4 },
                { imgUrl: "", text: "회색", score: 5 }
            ],
            pageNum : 2
        },
        {   // Q3 연령체크
            questions : "최근 3일 간 변의 색깔에 대해서 선택해 주세요.",
            answer : [
                { imgUrl: "", text: "10대", score: 1 },
                { imgUrl: "", text: "20대", score: 1 },
                { imgUrl: "", text: "30대", score: 1 },
                { imgUrl: "", text: "40대", score: 4 },
                { imgUrl: "", text: "50대", score: 4 },
                { imgUrl: "", text: "60대 이상", score: 3 }
            ],
            pageNum : 3
        },
        {   // Q4 유산균 섭취량 체크
            questions : "일주일 동안 유산균을 얼마나 섭취하나요?",
            answer : [
                { imgUrl: "", text: "섭취 안함", score: 5 },
                { imgUrl: "", text: "주 1 ~ 2회 섭취", score: 4 },
                { imgUrl: "", text: "주 3회 섭취", score: 3 },
                { imgUrl: "", text: "주 4회 섭취", score: 2 },
                { imgUrl: "", text: "주 5회 섭취", score: 1 },
                { imgUrl: "", text: "매일 섭취", score: 1 }
            ],
            pageNum : 4
        },
        {   // Q5 하루에 마시는 물
            questions : "하루에 마시는 물의 양은 어느 정도인가요?",
            answer : [
                { imgUrl: "", text: "약 0.5L", score: 5 },
                { imgUrl: "", text: "약 1L", score: 4 },
                { imgUrl: "", text: "약 1.5L", score: 1 },
                { imgUrl: "", text: "약 2L", score: 1 },
                { imgUrl: "", text: "약 2.5L", score: 2 },
                { imgUrl: "", text: "약 3L 이상", score: 4 }
            ],
            pageNum : 5
        },
        {   // Q6 이상 증상
            questions : "다음 중 최근 6개월 내에 아래의 증상이 있었나요?",
            answer : [
                { text: "복부 팽만감", score: 5 },
                { text: "복통", score: 5 },
                { text: "체중 감소", score: 5 },
                { text: "잦은 방귀", score: 5 },
                { text: "용종 제거", score: 5 },
                { text: "해당 없음", score: 1 },
            ],
            pageNum : 6
        },
    ]

    // 토탈 점수 함수
    const totalScore = score => {
        if ( score <= 15 ) {
            return "당신의 장은 건강합니다!"
        } else {
            return "장 건강이 좋지 않군요!"
        }
    }

    return (
        <div>
        </div>
    )
}

export default AnalTestComponent